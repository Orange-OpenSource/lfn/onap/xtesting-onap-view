#!/usr/bin/env python

import os
import shutil
import logging

from pygerrit2 import GerritRestAPI

logging.basicConfig(format="%(asctime)s %(levelname)s %(message)s",
                    level=logging.INFO)

logger = logging.getLogger("clean_folders")
rest = GerritRestAPI(url='https://gerrit.onap.org/r/')

open_reviews = []
stale_reviews = []
results_to_delete = []
reviews_max_patchset = {}
reviews_several_patchsets = []
reviews_white_list = ['onap_daily_pod4_master']

projects = ["oom", "so", "aai/oom", "testsuite/oom", "clamp"]
subfolders = sorted([ f.path for f in os.scandir(
    "{}/results/".format(os.getcwd())) if f.is_dir() ])

for project in projects:
    changes = rest.get("/changes/?q=project:{}%20status:open".format(project))
    for change in changes:
        open_reviews.append(change['_number'])
    changes = rest.get("/changes/?q=age:1mon%20project:{}%20status:open".format(
        project))
    for change in changes:
        stale_reviews.append(change['_number'])

logger.debug("%s reviews opened", len(open_reviews))

for subfolder in subfolders:
    logger.debug("subfolder: %s", subfolder)
    try:
        review = int(subfolder.split('/')[-1].split('-')[0])
    except ValueError:
        review = str(subfolder.split('/')[-1].split('-')[0])
    patchset = int(subfolder.split('/')[-1].split('-')[1])
    logger.debug("review: %6s", review)
    logger.debug("patchset: %2s", patchset)
    if review in stale_reviews:
        logger.info("review %6s is in stale_reviews, marking for deletion",
                    review)
        results_to_delete.append(subfolder)
    else:
        if review not in open_reviews:
            logger.info(
                "review %s is in NOT open_reviews", review)
            if review not in reviews_white_list:
                logger.info("review NOT in white list, marking for deletion")
                results_to_delete.append(subfolder)
            else:
                logger.info("Review found in white list, keep it")
        else:
            logger.debug(
                "review %6s is in open_reviews, looking for most recent patchset",
                review)
            if review in reviews_max_patchset:
                logger.debug("review %6s has several patchsets, finding max",
                             review)

                if review not in reviews_several_patchsets:
                    logger.debug(
                        "review %6s has several patchsets, adding to list",
                        review)
                    reviews_several_patchsets.append(review)
                if reviews_max_patchset[review] < patchset:
                    logger.debug(
                        "current max patchset (%2s) for review %6s is smaller than this one: %2s",
                        reviews_max_patchset[review], review, patchset)
                    reviews_max_patchset[review] = patchset
            else:
                logger.debug("patchset %2s for review %6s is the first one",
                             patchset, review)
                reviews_max_patchset[review] = patchset

logger.debug("finding subfolders with old patchsets")
for subfolder in subfolders:
    logger.debug("subfolder: %s", subfolder)
    try:
        review = int(subfolder.split('/')[-1].split('-')[0])
    except ValueError:
        review = str(subfolder.split('/')[-1].split('-')[0])

    patchset = int(subfolder.split('/')[-1].split('-')[1])
    logger.debug("review: %6s", review)
    logger.debug("patchset: %2s", patchset)
    if review in reviews_several_patchsets:
        logger.debug(
            "review %6s has several patchset, keeping only the most recent",
            review)
        if patchset < reviews_max_patchset[review]:
            logger.info(
                "current patchset (%2s) of review %6s is smaller than most recent (%2s), adding to deletion",
                patchset, review, reviews_max_patchset[review])
            results_to_delete.append(subfolder)

for subfolder in results_to_delete:
    logger.info("deleting folder %s", subfolder)
    shutil.rmtree(subfolder)
